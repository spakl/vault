resource "vault_jwt_auth_backend" "gitlab" {
    description         = "Demonstration of the Terraform JWT auth backend"
    path                = "jwt"
    oidc_discovery_url  = "https://gitlab.com/"
    bound_issuer        = "https://gitlab.com/"
}

resource "vault_jwt_auth_backend_role" "certs" {
  backend         = vault_jwt_auth_backend.gitlab.path
  role_type       = "jwt"
  role_name       = "test-role"
  user_claim      = "user_email"
  token_policies  = ["default", "certs_admin"]

  bound_claims = {
    projectId = "red,green,blue"
  }
}