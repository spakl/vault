terraform {
  required_providers {
    vault = {
      source = "hashicorp/vault"
      version = "3.24.0"
    }
  }
  backend "http"{}
}

provider "vault" {
  # Configuration options
  address = "https://vault.svc.spakl:8200"
  skip_tls_verify = true

}

data "vault_auth_backend" "oidc" {
  path = "oidc"
}
