## Create Gitlab App

Create App Doc
https://docs.gitlab.com/ee/integration/vault.html

Token Claims and INFO

https://docs.gitlab.com/ee/ci/secrets/id_token_authentication.html



## Setup Gitlab OIDC

| Field | Value | 
|---|---|
| OIDC Discovery URL | https://gitlab.com |
| OIDC Client ID | 6df2e8e1084a046fd1c7d3de8b833bf7b40ad6c635bcdc247a4f3ae021d3e2c8 |
| Bound issuer | https://vault.svc.spakl |




## Create auth admin role 
vault write -tls-skip-verify auth/oidc/role/admin - <<EOF
{
   "user_claim": "sub",
   "allowed_redirect_uris": "https://vault.svc.spakl:8200/ui/vault/auth/oidc/oidc/callback",
   "bound_audiences": "6df2e8e1084a046fd1c7d3de8b833bf7b40ad6c635bcdc247a4f3ae021d3e2c8",
   "oidc_scopes": "openid",
   "role_type": "oidc",
   "policies": ["deployer","default","admin"],
   "ttl": "1h",
   "bound_claims": { "groups": ["spakl"] }
}
EOF

## Create auth admin role 
vault write -tls-skip-verify auth/oidc/role/deployer - <<EOF
{
   "user_claim": "iss",
   "allowed_redirect_uris": "https://vault.svc.spakl:8200/ui/vault/auth/oidc/oidc/callback",
   "bound_audiences": "6df2e8e1084a046fd1c7d3de8b833bf7b40ad6c635bcdc247a4f3ae021d3e2c8",
   "oidc_scopes": "openid",
   "role_type": "oidc",
   "policies": ["deployer","default","admin"],
   "ttl": "1h",
   "bound_claims": { "project_id": "45870422" }
}
EOF


## JWT
vault write -tls-skip-verify auth/gitlab-jwt/role/deployer - <<EOF
{
  "role_type": "jwt",
  "policies": ["deployer","default","admin"],
  "token_explicit_max_ttl": 60,
  "user_claim": "user_email",
  "bound_claims_type": "glob",
  "bound_claims": {
    "namespace_path": "spakl/*"
  }
}
EOF